from selenium.webdriver.common.by import By
class ProjectPage:

    def __init__(self,browser):
        self.browser = browser

    def load(self):
        enter = self.browser.find_element(By.CSS_SELECTOR,'.icon_tools')
        enter.click()
    def add_project(self):
        add_project_button = self.browser.find_element(By.CSS_SELECTOR, '[href="http://demo.testarena.pl/administration/add_project"]')
        add_project_button.click()

    def fill_fields(self,name,prefix,description):
        name_field = self.browser.find_element(By.CSS_SELECTOR, '#name')
        name_field.send_keys(name)
        prefix_field = self.browser.find_element(By.CSS_SELECTOR, '#prefix')
        prefix_field.send_keys(prefix)
        description_field = self.browser.find_element(By.CSS_SELECTOR, '#description')
        description_field.send_keys(description)
        save_button = self.browser.find_element(By.CSS_SELECTOR, '#save')
        save_button.click()

    def fill_search_bar_and_execute(self,ProjectName):
        search_bar_field = self.browser.find_element(By.CSS_SELECTOR, '#search')
        search_bar_field.send_keys(ProjectName)
        search_button = self.browser.find_element(By.CSS_SELECTOR, '#j_searchButton')
        search_button.click()








