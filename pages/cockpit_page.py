from selenium.webdriver.common.by import By
from selenium.webdriver.common.devtools.v107 import browser


class CockpitPage:
    URL = 'http://demo.testarena.pl/zaloguj'
    addon = '/moje_wiadomosci'
    def __init__(self,browser):
        self.browser = browser

    def load(self):
        self.browser.get(self.URL+self.addon)
        envelope = browser.find_element(By.CSS_SELECTOR, '.top_messages')
        envelope.click()
