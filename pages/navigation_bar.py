from selenium.webdriver.common.by import By
class NavigationBar:
    def __init__(self,browser):
        self.browser = browser

    def go_to_main_menu(self):
        header_logo = self.browser.find_element(By.CSS_SELECTOR, '#header_logo')
        header_logo.click()
    def go_to_projects(self):
        projects_icon = self.browser.find_element(By. CSS_SELECTOR, '[href="http://demo.testarena.pl/UJMS01/project_view"]')
        projects_icon.click()