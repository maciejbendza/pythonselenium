import time
from telnetlib import EC

from selenium.webdriver import Chrome
from selenium.webdriver.common.by import By
from selenium.webdriver.support.wait import WebDriverWait
from webdriver_manager.chrome import ChromeDriverManager
import pytest

from pages.cockpit_page import CockpitPage
from pages.login_page import LoginPage


@pytest.fixture()
def browser():
   browser = Chrome(executable_path=ChromeDriverManager().install())
   login_page = LoginPage(browser)

   login_page.load()
   login_page.login("administrator@testarena.pl", "sumXQQ72$L")

   yield browser
   browser.quit()
def test_logout_correctly_displayed(browser):
    assert browser.find_element(By.CSS_SELECTOR, '[title=Wyloguj]').is_displayed() is True











def test_open_administration(browser):
    cockpit_page = CockpitPage(browser)
    cockpit_page.click_administration()
    admin_button = browser.find_element(By.CSS_SELECTOR, '[title=Administracja]')
    admin_button.click()

    assert browser.find_element(By.CSS_SELECTOR, '.content_title').text == 'Projekty'
