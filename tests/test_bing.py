import time

from selenium.webdriver import Chrome
from selenium.webdriver.common.by import By
from webdriver_manager.chrome import ChromeDriverManager
def test_searching_in_duckduckgo_in_bing():
    # Uruchomienie przeglądarki Chrome. Ścieżka do chromedrivera
    # ustawiana automatycznie przez bibliotekę webdriver-manager
    browser = Chrome(executable_path=ChromeDriverManager().install())

    # Otwarcie strony duckduckgo
    browser.get('https://bing.com/')


    # Znalezienie paska wyszukiwania
    search_bar = browser.find_element(By.CSS_SELECTOR, "[name='q']")
    # Znalezienie guzika wyszukiwania (lupki)
    search_button = browser.find_element(By.CSS_SELECTOR, "#search_icon")

    # Asercje że elementy są widoczne dla użytkownika
    assert search_bar.is_displayed() is True
    assert search_button.is_displayed() is True
    # Szukanie Vistula University
    search_bar.send_keys("Vistula University")
    search_button.click()
    # Sprawdzenie że lista wyników jest dłuższa niż 2
    search_results = browser.find_elements(By.CSS_SELECTOR,".b_algo")
    assert len(search_results) > 2
    # Sprawdzenie że jeden z tytułów to strona uczelni
    title = browser.find_elements(By.CSS_SELECTOR,'.b_topTitle')
    first_title_text = title[0].text
    assert first_title_text == "Home - Vistula University"
